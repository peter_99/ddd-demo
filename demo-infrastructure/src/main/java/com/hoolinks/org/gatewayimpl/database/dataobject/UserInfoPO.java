package com.hoolinks.org.gatewayimpl.database.dataobject;

import com.baomidou.mybatisplus.annotation.TableName;
import com.hoolinks.domain.org.user.UserInfo;
import com.hoolinks.domain.org.user.UserInfoStatusEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户信息
 * </p>
 *
 * @author zzp
 * @since 2021-07-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_user_info")
public class UserInfoPO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 登录名
     */
    private String loginId;

    /**
     * 电话号码
     */
    private String phone;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 状态，1表示启用，0表示禁用
     */
    private Integer status;

    public static UserInfoPO newPO(UserInfo userInfo) {
        UserInfoPO po = new UserInfoPO();
        Date nowDate = new Date();
        BeanUtils.copyProperties(userInfo, po);
        po.setCreateTime(nowDate);
        po.setUpdateTime(nowDate);
        po.setStatus(UserInfoStatusEnum.ENABLE.getId());

        return po;
    }

    public static UserInfoPO updatePO(UserInfo userInfo, UserInfoPO po) {
        Date nowDate = new Date();
        po.setUserName(userInfo.getUserName());
        po.setLoginId(userInfo.getLoginId());
        po.setPhone(userInfo.getPhone());
        po.setStatus(userInfo.getStatus());
        po.setUpdateTime(nowDate);

        return po;
    }

    public UserInfo toUserInfo() {
        UserInfo info = new UserInfo();
        BeanUtils.copyProperties(this, info);

        return info;
    }
}
