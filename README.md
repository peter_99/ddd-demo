# ddd-demo

## 介绍
DDD 领域驱动设计示例项目，源于阿里COLA进行简化

# COLA架构

## COLA 概述

**架构**的**意义** 就是 要素结构：

- 要素 是 组成架构的重要元素；
- 结构 是 要素之间的关系。

而 **应用架构**的**意义** 就在于

- 定义一套良好的结构；
- 治理应用复杂度，降低系统熵值；
- 从随心所欲的混乱状态，走向井井有条的有序状态。

![archWhy](https://img-blog.csdnimg.cn/20201209182220206.png)

COLA架构就是为此而生，其核心职责就是定义良好的应用结构，提供最佳应用架构的最佳实践。通过不断探索，我们发现良好的分层结构，良好的包结构定义，可以帮助我们治理混乱不堪的业务应用系统。

![cure](https://img-blog.csdnimg.cn/2020120918285068.png)

经过多次迭代，我们定义出了相对稳定、可靠的应用架构：COLA 4.0

![cola](https://img-blog.csdnimg.cn/20201209182934838.png)


# 版本迭代

## 4.0.0 版本

https://blog.csdn.net/significantfrank/article/details/110934799

## 3.1.0 版本

https://blog.csdn.net/significantfrank/article/details/109529311

1. 进一步简化了`cola-core`，只保留了扩展能力。
2. 将`exception`从`cola-core`移入到`cola-common`。
3. 对`archetype`中的分包逻辑进行重构，改成按照`domain`做划分。
4. 将`cola-archetype-web`中的`controller`改名为`adapter`，为了呼应六边形架构的命名。

## 3.0.0 版本

https://blog.csdn.net/significantfrank/article/details/106976804

## 2.0.0 版本

https://blog.csdn.net/significantfrank/article/details/100074716

## 1.0.0 版本

https://blog.csdn.net/significantfrank/article/details/85785565

# 工具
## Aliyun Java Initializr
阿里的应用脚手架，用于初始化一个多模块maven应用，有标准的CRUD和COLA

https://start.aliyun.com/bootstrap.html

# 阅读
## 如何写复杂业务代码？

https://mp.weixin.qq.com/s/pdjlf9I73sXDr30t-5KewA

## 公众号
关于COLA的更多信息，请关注微信公众号：

![qrcode_60.jpg](https://img-blog.csdnimg.cn/2020110314110321.png#pic_center)