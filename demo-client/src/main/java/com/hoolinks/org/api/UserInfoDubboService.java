package com.hoolinks.org.api;

import com.alibaba.cola.exception.BizException;
import com.hoolinks.org.dto.clientobject.UserInfoVO;

import java.util.List;

/**
 * <p>
 *  用户信息 服务类
 * </p>
 *
 * @author zzp
 * @since 2021-07-11
 */
public interface UserInfoDubboService {

    void save(UserInfoVO userInfoVo) throws BizException;

    void delete(List<Integer> userIds) throws BizException;

    void batchUpdateStatus(List<Integer> userIds, Integer status) throws BizException;
}
