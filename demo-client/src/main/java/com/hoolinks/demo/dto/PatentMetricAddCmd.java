package com.hoolinks.demo.dto;

import com.hoolinks.demo.dto.clientobject.PatentMetricCO;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * PatentMetricAddCmd
 *
 * @author Frank Zhang
 * @date 2019-03-03 11:37 AM
 */
@Data
public class PatentMetricAddCmd extends com.hoolinks.demo.dto.CommonCommand {
    @NotNull
    private PatentMetricCO patentMetricCO;
}
