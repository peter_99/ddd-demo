package com.hoolinks.app.org.service.impl;

import com.alibaba.cola.exception.BizException;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.hoolinks.app.org.service.IUserInfoService;
import com.hoolinks.domain.org.user.UserInfo;
import com.hoolinks.domain.org.user.UserInfoStatusEnum;
import com.hoolinks.org.gatewayimpl.database.dataobject.UserInfoPO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description 用户信息 校验器
 * @Author zzp
 * @since 2021.07.11
 **/
@Component
public class UserInfoServiceValidator {

    @Autowired
    private IUserInfoService userInfoService;

    public void saveValidate(UserInfo userInfo) {
        if (StringUtils.isBlank(userInfo.getUserName())) {
            throw new BizException("用户名不能为空");
        }

        if (StringUtils.isBlank(userInfo.getLoginId())) {
            throw new BizException("登录名不能为空");
        }
    }

    public String saveValidate2(UserInfo userInfo) {
        if (StringUtils.isBlank(userInfo.getUserName())) {
            return "用户名不能为空";
        }

        if (StringUtils.isBlank(userInfo.getLoginId())) {
            return "登录名不能为空";
        }

        return "";
    }

    public void delValidate(List<Integer> userIds) {
        if (CollectionUtils.isEmpty(userIds)) {
            throw new BizException("用户id列表不能为空");
        }

        for (Integer userId : userIds) {
            UserInfoPO userInfo = userInfoService.getById(userId);

            if (!userInfo.toUserInfo().isDel()) {
                throw new BizException("存在不能删除的用户");
            }
        }
    }

    public void updateStatusValidate(Integer status) {
        if (status == null) {
            throw new BizException("修改的状态不能为空");
        }

        if (UserInfoStatusEnum.getById(status) == null) {
            throw new BizException("状态在系统不存在");
        }
    }

}
