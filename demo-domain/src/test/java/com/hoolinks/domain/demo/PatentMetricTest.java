package com.hoolinks.domain.demo;

import com.hoolinks.domain.demo.metrics.techinfluence.AuthorType;
import com.hoolinks.domain.demo.metrics.techinfluence.InfluenceMetric;
import com.hoolinks.domain.demo.metrics.techinfluence.PatentMetric;
import com.hoolinks.domain.demo.metrics.techinfluence.PatentMetricItem;
import com.hoolinks.domain.demo.metrics.techinfluence.*;
import com.hoolinks.domain.demo.user.UserProfile;
import org.junit.Assert;
import org.junit.Test;

/**
 * PatentMetricTest
 *
 * @author Frank Zhang
 * @date 2019-02-26 4:20 PM
 */
public class PatentMetricTest {

    @Test
    public void testPatentMetric(){
        PatentMetric patentMetric = new PatentMetric(new InfluenceMetric(new UserProfile()));
        patentMetric.addMetricItem(new PatentMetricItem("patentName","patentDesc","patentNo","sharingLink", AuthorType.FIRST_AUTHOR));
        patentMetric.addMetricItem(new PatentMetricItem("patentName","patentDesc","patentNo","sharingLink", AuthorType.OTHER_AUTHOR));

        Assert.assertEquals(25, patentMetric.calculateScore(), 0.01);

    }
}
