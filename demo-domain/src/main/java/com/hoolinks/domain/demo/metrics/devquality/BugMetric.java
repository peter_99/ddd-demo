package com.hoolinks.domain.demo.metrics.devquality;

import com.hoolinks.domain.demo.metrics.*;
import com.hoolinks.domain.demo.metrics.SubMetric;
import com.hoolinks.domain.demo.metrics.SubMetricType;
import com.hoolinks.domain.demo.user.Role;


/**
 * BUG数指标
 */
public class BugMetric extends SubMetric {

    public BugMetric(){
        this.subMetricType = SubMetricType.Bug;
    }

    @Override
    public double getWeight() {
        return metricOwner.getWeight().getUnanimousWeight();
    }

    @Override
    public double calculateScore() {
        if(metricOwner.getRole() == Role.OTHER){
            return 0;
        }
        return super.calculateScore();
    }
}
