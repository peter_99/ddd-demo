package com.hoolinks.domain.demo.metrics.appquality;

import com.hoolinks.domain.demo.metrics.MainMetric;
import com.hoolinks.domain.demo.metrics.MainMetricType;
import com.hoolinks.domain.demo.user.UserProfile;

public class AppQualityMetric extends MainMetric {

    private AppMetric appMetric;

    public AppQualityMetric(UserProfile metricOwner){
        this.metricOwner = metricOwner;
        metricOwner.setAppQualityMetric(this);
        this.metricMainType = MainMetricType.APP_QUALITY;
    }

    @Override
    public double getWeight() {
        return metricOwner.getWeight().getAppQualityWeight();
    }
}
