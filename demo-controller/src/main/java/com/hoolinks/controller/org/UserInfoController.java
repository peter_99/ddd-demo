package com.hoolinks.controller.org;


import com.alibaba.cola.exception.BizException;
import com.hoolinks.app.org.service.IUserInfoService;
import com.hoolinks.common.exception.ErrorCode;
import com.hoolinks.org.dto.clientobject.UserInfoOperationVO;
import com.hoolinks.org.dto.clientobject.UserInfoVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.alibaba.cola.dto.Response;

/**
 * <p>
 *  用户信息 前端控制器
 * </p>
 *
 * @author zzp
 * @since 2021-07-11
 */
@RestController
@RequestMapping("/user-info")
public class UserInfoController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private IUserInfoService userInfoService;

    @ResponseBody
    @RequestMapping(value = "save", method = RequestMethod.POST)
    public Response save(@RequestBody UserInfoVO userInfoVO) {
        try {
            userInfoService.save(userInfoVO);
        } catch (BizException e) {
            logger.error("保存接口出现业务异常，异常信息为", e);
            return Response.buildFailure(ErrorCode.BIZ_ERROR.getErrCode(), e.getMessage());
        } catch (Exception e) {
            logger.error("保存接口出现系统异常，异常信息为", e);
            return Response.buildFailure(ErrorCode.SYS_ERROR.getErrCode(), "保存异常");
        }
        return Response.buildSuccess();
    }

    @ResponseBody
    @RequestMapping(value = "del", method = RequestMethod.POST)
    public Response del(@RequestBody UserInfoOperationVO operationVo) {
        try {
            userInfoService.delete(operationVo.getIds());
        } catch (BizException e) {
            logger.error("删除接口出现业务异常，异常信息为", e);
            return Response.buildFailure(ErrorCode.BIZ_ERROR.getErrCode(), e.getMessage());
        } catch (Exception e) {
            logger.error("删除接口出现系统异常，异常信息为", e);
            return Response.buildFailure(ErrorCode.SYS_ERROR.getErrCode(), "删除失败");
        }

        return Response.buildSuccess();
    }

    @ResponseBody
    @RequestMapping(value = "status/update", method = RequestMethod.POST)
    public Response updateStatus(@RequestBody UserInfoOperationVO operationVo) {
        try {
            userInfoService.batchUpdateStatus(operationVo.getIds(), operationVo.getStatus());
        } catch (BizException e) {
            logger.error("更新状态接口出现业务异常，异常信息为", e);
            return Response.buildFailure(ErrorCode.BIZ_ERROR.getErrCode(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("更新状态接口出现系统异常，异常信息为", e);
            return Response.buildFailure(ErrorCode.SYS_ERROR.getErrCode(), "删除失败");
        }

        return Response.buildSuccess();
    }

}
